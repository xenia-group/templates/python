#!/usr/bin/env python3

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

package_name = "package_name"
description = "Example description."

setup(
    name = package_name,
    version = "0.0.1",
    author = "Various Contributors",
    author_email = "contact@xenialinux.com",
    description= (description),
    license = "GPL-2",
    keywords = "",
    url = f"https://gitlab.com/xenia-group/{package_name}",
    packages = [package_name],
    requires=[],
    entry_points={
        'console_scripts': [
            f'{package_name} = {package_name}.{package_name}:main',
        ],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Package"
        "License :: OSI Approved :: GPL-3"
    ],
)
